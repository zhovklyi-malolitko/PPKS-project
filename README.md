# Project Task Tracker

## Backend setup
```
pip install flask flask_restful flask_cors pandas
```

## Run the server on python backend (http://localhost:5000)
```
python main.py
```

## Frontend setup
```
npm install
```

## Run the Vue dev server (http://localhost:8080)
```
npm run serve
```